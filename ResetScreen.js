/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';

const ResetScreen = ({navigation}) => {
  const [email, setEmail] = React.useState('');
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F7F7F7',
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Image
        onPress={() => navigation.navigate.go.Back('LoginScreen')}
        style={{
          alignSelf: 'flex-start',
          marginTop: 25,
        }}
        source={require('./assets/back-button.png')}
      />
      <View
        style={{
          width: '100%',
          weight: 360,
          height: 640,
          backgroundColor: '#F4F4F4',
          alignItems: 'center',
        }}>
        <Image
          style={{
            justifyContent: 'center',
            marginTop: 120,
            marginBottom: 38,
          }}
          source={require('./assets/marlin1.png')}
        />
        <TextInput
          style={{
            width: 273,
            height: 43,
            backgroundColor: '#FFFFFF',
            borderColor: '#002558',
            marginBottom: 25,
            justifyContent: 'space-around',
          }}
          placeholder="Email"
          label="Email"
          value={email}
          onChangeText={email => setEmail(email)}
        />
        <Text style={{color: '#002558'}}></Text>
        <TouchableOpacity
          onPress={() => Alert.alert('Periksa Email Anda!')}
          style={{
            backgroundColor: '#2E3283',
            width: 273,
            height: 43,
            alignItems: 'center',
            justifyContent: 'center',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
          }}>
          <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
            Request Reset
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ResetScreen;
