/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Style,
  Button,
  Navigation,
  Alert,
} from 'react-native';

const LoginScreen = ({navigation}) => {
  const [name, setName] = React.useState('');
  const [pass, setPassword] = React.useState('');
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F7F7F7',
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          width: '100%',
          weight: 360,
          height: 640,
          alignItems: 'center',
        }}>
        <Image
          style={{
            justifyContent: 'center',
            marginTop: 50,
            marginBottom: 50,
          }}
          source={require('./assets/marlin2.png')}
        />
        <TextInput
          style={{
            width: 273,
            height: 43,
            backgroundColor: '#FFFFFF',
            borderColor: '#002558',
            marginBottom: 10,
          }}
          placeholder="Name"
          label="Name"
          value={name}
          onChangeText={name => setName(name)}
        />
        <Text style={{color: '#002558'}}></Text>
        <TextInput
          style={{
            width: 273,
            height: 43,
            backgroundColor: '#FFFFFF',
            borderColor: '#002558',
            marginBottom: 10,
            justifyContent: 'space-around',
          }}
          placeholder="Password"
          label="Password"
          value={pass}
          onChangeText={pass => setPassword(pass)}
        />
        <Text style={{color: '#002558'}}></Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('HomeScreen')}
          style={{
            backgroundColor: '#2E3283',
            width: 273,
            height: 43,
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 5,
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
          }}>
          <Text style={{color: '#FFFFFF', textAlign: 'left'}}>Sign in</Text>
        </TouchableOpacity>
        <Text
          onPress={() => navigation.navigate('ResetScreen')}
          style={{color: '#2E3283', fontSize: 13}}>
          Forgot Password
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 30,
            marginTop: 40,
          }}>
          <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          <View>
            <Text
              style={{
                width: '100%',
                textAlign: 'center',
                fontSize: 13,
                color: '#2E3283',
                paddingHorizontal: 10,
                marginBottom: 10,
              }}>
              Login With
            </Text>
          </View>
          <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}>
          <TouchableOpacity onPress={() => {}}>
            <Image
              style={{
                marginTop: 20,
              }}
              source={require('./assets/icon_fb.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Image
              style={{
                marginTop: 20,
                marginLeft: 50,
              }}
              source={require('./assets/icon_google.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 13,
            color: 'grey',
            marginTop: -75,
          }}>
          App Version 2.8.3
        </Text>
      </View>
      <View style={styles.footer}>
        <Text
          style={{
            padding: 10,
            textAlign: 'center',
            alignItems: 'center',
            backgroundColor: '2E3283',
          }}>
          Don't have an account?
          <TouchableOpacity
            onPress={() => navigation.navigate('RegisterScreen')}>
            <Text style={{fontWeight: 'bold'}}> Sign Up</Text>
          </TouchableOpacity>
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    backgroundColor: 'white',
    height: 50,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
});

export default LoginScreen;
