/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  Alert,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F7F7F7',
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          width: '100%',
          weight: 360,
          height: 640,
          backgroundColor: '#F4F4F4',
          alignItems: 'center',
        }}>
        <Image
          style={{
            justifyContent: 'center',
          }}
          source={require('./assets/MarlinSampleBannerBanner.png')}
        />
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}>
          <TouchableOpacity onPress={() => {}}>
            <Image
              style={{
                marginTop: 50,
                marginLeft: 25,
              }}
              source={require('./assets/ic_ferry_intl.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Image
              style={{
                marginTop: 50,
                marginLeft: 25,
              }}
              source={require('./assets/ic_ferry_domestic.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Image
              style={{
                marginTop: 50,
                marginLeft: 25,
              }}
              source={require('./assets/ic_attraction.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <Image
              style={{
                marginTop: 50,
              }}
              source={require('./assets/ic_pioneership.png')}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('LoginScreen')}
          style={{
            backgroundColor: '#2E3283',
            width: 108,
            height: 40,
            alignItems: 'center',
            justifyContent: 'center',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
          }}>
          <Text style={{color: '#FFFFFF', textAlign: 'left'}}>More ...</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

// function MyBookingScreen() {
//   return (
//     <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
//       <Text>My Booking!</Text>
//     </View>
//   );
// }

// function HelpScreen() {
//   return (
//     <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
//       <Text>Help</Text>
//     </View>
//   );
// }

// function ProfileScreen() {
//   return (
//     <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
//       <Text>Profile</Text>
//     </View>
//   );
// }

// const getIcon = label => {
//   switch (label) {
//     case 'Home':
//       return require('./assets/Home.png');
//     case 'My Booking':
//       return require('./assets/Booking.png');
//     case 'Help':
//       return require('./assets/Question.png');
//     case 'Profile':
//       return require('./assets/User.png');
//   }
// };

// const Tab = createBottomTabNavigator();

// function MyTabs() {
//   return (
//     <Tab.Navigator
//       tabBar={props => <MyTabBar {...props} />}
//       screenOptions={{headerShown: false}}>
//       <Tab.Screen name="Home" component={HomeScreen} />
//       <Tab.Screen name="My Booking" component={MyBookingScreen} />
//       <Tab.Screen name="Help" component={HelpScreen} />
//       <Tab.Screen name="Profile" component={ProfileScreen} />
//     </Tab.Navigator>
//   );
// }

// const Home = () => {
//   return <MyTabs />;
// };

export default HomeScreen;
