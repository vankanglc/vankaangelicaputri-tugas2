/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';

const RegisterScreen = ({navigation}) => {
  const [name, setName] = React.useState('');
  const [pass, setPassword] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [phone, setPhone] = React.useState('');
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F7F7F7',
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          width: '100%',
          weight: 360,
          height: 640,
          alignItems: 'center',
        }}>
        <Image
          style={{
            justifyContent: 'center',

            marginTop: 90,
            marginBottom: 30,
          }}
          source={require('./assets/marlin3.png')}
        />
        <TextInput
          style={{
            width: 273,
            height: 43,
            backgroundColor: '#FFFFFF',
            borderColor: '#002558',
            marginBottom: 10,
          }}
          placeholder="Name"
          label="Name"
          value={name}
          onChangeText={name => setName(name)}
        />
        <Text style={{color: '#002558'}}></Text>
        <TextInput
          style={{
            width: 273,
            height: 43,
            backgroundColor: '#FFFFFF',
            borderColor: '#002558',
            marginBottom: 10,
          }}
          placeholder="Email"
          label="Email"
          value={email}
          onChangeText={email => setEmail(email)}
        />
        <Text style={{color: '#002558'}}></Text>
        <TextInput
          style={{
            width: 273,
            height: 43,
            backgroundColor: '#FFFFFF',
            borderColor: '#002558',
            marginBottom: 10,
          }}
          placeholder="Phone"
          label="Phone"
          value={phone}
          onChangeText={phone => setPhone(phone)}
        />
        <Text style={{color: '#002558'}}></Text>
        <TextInput
          style={{
            width: 273,
            height: 43,
            backgroundColor: '#FFFFFF',
            borderColor: '#002558',
            marginBottom: 10,
          }}
          placeholder="Password"
          label="Password"
          value={pass}
          onChangeText={pass => setPassword(pass)}
        />
        <Text style={{color: '#002558'}}></Text>
        <TouchableOpacity
          onPress={() => Alert.alert('Register Berhasil!')}
          style={{
            backgroundColor: '#2E3283',
            width: 273,
            height: 43,
            alignItems: 'center',
            justifyContent: 'center',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
          }}>
          <Text style={{color: '#FFFFFF', textAlign: 'left'}}>Sign up</Text>
        </TouchableOpacity>
        <View style={styles.footer}>
          <Text
            style={{
              padding: 10,
              textAlign: 'center',
              alignItems: 'center',
              backgroundColor: '2E3283',
            }}>
            Already have account?
            <TouchableOpacity
              onPress={() => navigation.navigate('LoginScreen')}>
              <Text style={{fontWeight: 'bold'}}> Log in</Text>
            </TouchableOpacity>
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    backgroundColor: 'white',
    height: 50,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
});

export default RegisterScreen;
